import React, { Component } from "react";
import { connect } from "react-redux";

class ThongTinTroChoi extends Component {
  renderListXucXac = () => {
    return this.props.mangXucXac.map((item, index) => {
      return (
        <img
          src={item.img}
          alt=""
          key={index}
          style={{ width: 50, margin: 15 }}
        />
      );
    });
  };
  render() {
    return (
      <div>
        <h2 className="text-dark pt-3">Game Đổ Xúc Xắc</h2>
        <div className="d-flex justify-content-center pt-5">
          <button
            class="btn btn-success mx-5 "
            style={{
              width: 150,
              height: 150,
              fontSize: 30,
              borderRadius: 6,
            }}
            onClick={() => {
              this.props.handleChoice(true);
            }}
          >
            TÀI
          </button>
          <div>{this.renderListXucXac()}</div>
          <button
            class="btn btn-success mx-5 "
            style={{
              width: 150,
              height: 150,
              fontSize: 30,
              borderRadius: 6,
            }}
            onClick={() => {
              this.props.handleChoice(false);
            }}
          >
            XỈU
          </button>
        </div>
      </div>
    );
  }
}
// lấy dữ liệu từ redux render ra giao diện
let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChoice: (taiXiu) => {
      let action = {
        type: "LUA_CHON",
        payload: taiXiu,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ThongTinTroChoi);
