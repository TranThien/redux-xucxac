import React, { Component } from "react";
import KetQua from "./KetQua";
import ThongTinTroChoi from "./ThongTinTroChoi";
import bg_game from "../asset/bgGame.png";
import "../game.css";

export default class XucXac extends Component {
  render() {
    return (
      <div
        style={{
          width: "100vw",
          height: "100vh",
          backgroundImage: `url(${bg_game})`,
        }}
        className="bg_game"
      >
        <ThongTinTroChoi />
        <KetQua />
      </div>
    );
  }
}
