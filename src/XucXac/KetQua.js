import React, { Component } from "react";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    console.log(this.props.choice);
    return (
      <div className="pt-5">
        <h2>Bạn chọn : {this.props.choice ? "TÀI" : "XỈU"}</h2>
        <h3 className="pt-2">Số lượt bạn thắng : {this.props.win} </h3>
        <h3 className="pt-2">Số lượt bạn đã chơi : {this.props.total} </h3>
        <button
          onClick={this.props.handlePlayGame}
          className="btn btn-success p-3 pt-2 "
          style={{ fontSize: 36 }}
        >
          Play Game
        </button>
      </div>
    );
  }
}
// lấy dữ liệu từ redux xuống
const mapStateToProps = (state) => ({
  choice: state.xucXacReducer.choices,
  win: state.xucXacReducer.numberWin,
  total: state.xucXacReducer.totalNumberPlay,
});
const mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      let action = {
        type: "PLAY_GAME",
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
