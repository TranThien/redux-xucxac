const initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  choices: true, // true là tài , false là xỉu
  numberWin: 0,
  totalNumberPlay: 0,
};
export const xucXacReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LUA_CHON":
      {
        //gán lại giá trị cho state
        state.choices = action.payload;
      }
      return { ...state };

    case "PLAY_GAME": {
      // return mảng cũ thành mảng mới
      let newMangXucXac = state.mangXucXac.map((item) => {
        // random xúc xắc từ 0-5 nên phải cộng thêm 1
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${random}.png`,
          giaTri: random,
        };
      });
      state.mangXucXac = newMangXucXac;
      // Tổng số lượt chơi :
      state.totalNumberPlay += 1;
      //Tổng số lượt thắng
      const totalScore = state.mangXucXac.reduce((pre, cur) => {
        return pre + cur.giaTri;
      }, 0);
      if (
        (totalScore > 11 && state.choices === true) ||
        (totalScore <= 11 && state.choices === false)
      ) {
        state.numberWin += 1;
      }
      return { ...state };
    }

    default:
      return state;
  }
};
